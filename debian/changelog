hubicfuse (3.0.1-5) unstable; urgency=medium

  * If true/false are defined (C99), use stdbool.h instead of defining a
    bool type. Closes: #1011045.
  * Specify bash for hubic_token since it relies on it.
  * Standards-Version 4.6.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 16 May 2022 21:25:02 +0200

hubicfuse (3.0.1-4) unstable; urgency=medium

  * hubicfuse needs fusermount, so explicitly depend on fuse.
  * Switch to debhelper compatibility level 13.
  * Standards-Version 4.5.1, no change required.

 -- Stephen Kitt <skitt@debian.org>  Fri, 29 Jan 2021 16:08:20 +0100

hubicfuse (3.0.1-3) unstable; urgency=medium

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Stephen Kitt ]
  * Ensure variables are only declared once. Closes: #957346.
  * Set “Rules-Requires-Root: no”.
  * Standards-Version 4.5.0, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Thu, 21 May 2020 12:17:21 +0200

hubicfuse (3.0.1-2) unstable; urgency=medium

  * Migrate to Salsa.
  * Update debian/copyright.
  * Switch to debhelper compatibility level 11.
  * Standards-Version 4.1.4, no further change required.

 -- Stephen Kitt <skitt@debian.org>  Mon, 07 May 2018 11:29:45 +0200

hubicfuse (3.0.1-1) unstable; urgency=medium

  * New upstream release, merging all patches.
  * Update watch file.

 -- Stephen Kitt <skitt@debian.org>  Tue, 20 Jun 2017 08:32:50 +0200

hubicfuse (3.0.0-1) unstable; urgency=medium

  * New upstream release; this release allows empty containers to be
    removed again (Closes: #820327).
  * Add a manpage for hubicfuse.
  * Add more FUSE_USE_VERSION definitions to bug765288.diff.
  * Switch to debhelper compatibility level 10.

 -- Stephen Kitt <skitt@debian.org>  Mon, 07 Nov 2016 08:50:48 +0100

hubicfuse (2.1.0-2) unstable; urgency=medium

  * Add missing libmagic-dev build-dependency...

 -- Stephen Kitt <skitt@debian.org>  Fri, 02 Sep 2016 19:27:13 +0200

hubicfuse (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer (Closes: #820333); thanks to Sylvestre for his work!
  * Drop README.Debian, it duplicates the upstream README.
  * Clean up debian/control using cme.
  * Standards-Version 3.9.8, no change required.
  * Handle proxies in hubic_token.
  * Update debian/copyright.
  * Remove obsolete debian/NEWS.
  * Enable all hardening options.
  * Install hubic_token and its localisation file in /usr/share/hubicfuse,
    and document its availability in README.Debian (Closes: #820104).

 -- Stephen Kitt <skitt@debian.org>  Fri, 02 Sep 2016 13:29:41 +0200

hubicfuse (2.0.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Switch from libjson0-dev to libjson-0-dev (Closes: #815412)

 -- Neil Williams <codehelp@debian.org>  Sun, 21 Feb 2016 11:22:28 +0000

hubicfuse (2.0.0-1) unstable; urgency=medium

  * New upstream release
  * upload to unstable

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 28 Apr 2015 09:18:42 +0200

hubicfuse (1.3.1-1~exp1) experimental; urgency=medium

  * New upstream release
  * Standards-Version updated to 3.9.6

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 30 Dec 2014 14:25:38 +0100

hubicfuse (1.1.0-2) unstable; urgency=medium

  * Fix the Kfreebsd FTBFS. Thanks to Steven Chamberlain (Closes: #765288)

 -- Sylvestre Ledru <sylvestre@debian.org>  Wed, 22 Oct 2014 10:04:59 +0200

hubicfuse (1.1.0-1) unstable; urgency=medium

  * New upstream release

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 17 Aug 2014 15:54:13 +0200

hubicfuse (1.0-1) unstable; urgency=low

  * Initial release (Closes: #751141)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 06 Jun 2014 14:18:06 +0200
